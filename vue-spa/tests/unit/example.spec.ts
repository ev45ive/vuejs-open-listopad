import { shallowMount } from "@vue/test-utils";
import HelloWorld from "@/components/HelloWorld.vue";
import Vue from 'vue';

describe("HelloWorld.vue", () => {
  it("renders props.msg when passed", () => {
    const msg = "new message";

    // const mounted = new HelloWorld().$mount()
    // mounted.$el
    // mounted.medoa()
    // mounted.dane = 123
    // Render changes;
    // Vue.nextTick().then(()=>{})

    // https://vuejs.org/v2/guide/unit-testing.html#ad
    const wrapper = shallowMount(HelloWorld, {
      propsData: { msg }
    });
    expect(wrapper.text()).toMatch(msg);
  });
});
