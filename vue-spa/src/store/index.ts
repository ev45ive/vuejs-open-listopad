import Vue from "vue";
import Vuex from "vuex";
import { Playlist } from "@/models";
import SearchModule from "./SearchModule";

Vue.use(Vuex);

type State = {
  // playlists:{ [key:number]: Playlist },
  playlists: Record<number, Playlist>;
  selectedId: Playlist["id"] | null;
  loading: boolean;
};

function selectPlaylist(state: State, id: Playlist["id"]) {
  state.selectedId = id;
}

export default new Vuex.Store<{ playlists: State }>({
  modules: {
    search: SearchModule,
    playlists: {
      namespaced: true,
      state: {
        loading: false,
        playlists: {
          123: {
            id: 123,
            name: "Vue Hits!",
            favorite: true,
            color: "#ff00ff"
          },
          234: {
            id: 234,
            name: "Vue Top20!",
            favorite: false,
            color: "#ffff00"
          },
          345: {
            id: 345,
            name: "Best Vue!",
            favorite: false,
            color: "#00ffff"
          }
        },
        selectedId: null
      },
      mutations: {
        updatePlaylist(state, draft: Playlist) {
          state.playlists[draft.id] = draft;
        },
        selectPlaylist,
        setLoading(state, isLoading) {
          state.loading = isLoading;
        }
      },
      actions: {
        selectPlaylist(
          { state, commit, dispatch, getters },
          payload: Playlist
        ) {
          commit("selectPlaylist", payload.id);
        },
        savePlaylist({ state, commit, dispatch }, payload: Playlist) {
          commit("setLoading", true);
          setTimeout(() => {
            commit("updatePlaylist", payload);
            commit("setLoading", false);
            commit("selectPlaylist", payload.id);
          }, 2000);
        }
      },
      getters: {
        playlists(state) {
          return Object.keys(state.playlists).map(id => state.playlists[+id]);
        },
        selected(state) {
          return state.selectedId && state.playlists[state.selectedId];
        }
      }
    }
  }
});
