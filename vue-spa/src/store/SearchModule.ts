import { Album, AlbumsResponse } from "@/models/Album";
import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators";
import Axios from "axios";

@Module({
  namespaced: true
})
export default class SearchModule extends VuexModule {
  results: Album[] = [];
  query: string = "";
  error: string = "";
  loading: boolean = false;

  @Mutation
  updateQuery(query: string) {
    this.query = query;
  }

  @Mutation
  setError(error?: Error) {
    this.error = (error && error.message) || "";
  }

  @Mutation
  updateResults(results: Album[]) {
    this.results = results;
  }

  @Mutation
  setLoading(isLoading: boolean) {
    this.loading = isLoading;
  }

  @Action({})
  async searchAlbums(query: string) {
    try {
      this.context.commit("setError");
      this.context.commit("updateQuery", query);
      this.context.commit("setLoading", true);

      const resp = await Axios.get<AlbumsResponse>("search", {
        params: { type: "album", q: this.query }
      });
      const albums = resp.data.albums.items;
      this.context.commit("updateResults", albums);
      return albums;
    } catch (err) {
      this.context.commit("setError", err);
    } finally {
      this.context.commit("setLoading", false);
    }
  }

  get message() {
    return this.error;
  }

  // @Action({})
  // searchAlbums(query:string){
  //     this.context.commit('setError')
  //     this.context.commit('updateQuery',query)
  //     this.context.commit('setLoading',true)

  //     return Axios.get<AlbumsResponse>('search',{params:{type:'album', q:this.query}})
  //     .then(resp=>resp.data.albums.items)
  //     .then(albums => {
  //         this.context.commit('updateResults',albums)
  //         return albums
  //     })
  //     .catch(err => this.context.commit('setError',err))
  //     .finally(()=> this.context.commit('setLoading',false))
  // }
}
