import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import SearchField from "./components/SearchField.vue";
import Card from "./components/Card.vue";
import { auth } from "./services";

Vue.config.productionTip = false;

// <search-field/>
Vue.component("SearchField", SearchField);
Vue.component("Card", Card);

import axios, { AxiosStatic } from "axios";
import VueAxios from "vue-axios";
axios.defaults.baseURL = "https://api.spotify.com/v1/";

// https://github.com/dgrubelic/vue-authenticate
auth.getToken();

axios.interceptors.request.use(config => {
  config.headers["Authorization"] = `Bearer ${auth.getToken()}`;
  return config;
});

axios.interceptors.response.use(
  v => v,
  err => {
    if (err.response && err.response.status !== 200) {
      auth.authorize();
      return Promise.reject(err.response.data.error);
    }
    return Promise.reject(err);
  }
);

Vue.use(VueAxios, axios);
(window as any).app = new Vue({
  router,
  store,
  data: {
    user: { name: "Placki" }
  },
  // template:'<div>Ala ma kota</div>'
  render: h => h(App)
}).$mount("#app");

// Vue.prototype.axios = axios;
// Vue.prototype.$http = axios;

// declare module 'vue/types/vue' {
//   interface Vue {
//     axios: AxiosStatic;
//     $http: AxiosStatic;
//   }
// }
