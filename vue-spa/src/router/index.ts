import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "../views/Home.vue";
import Playlists from "@/views/Playlists.vue";
import MusicSearch from "@/views/MusicSearch.vue";

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: "/",
    redirect: "/playlists"
  },
  {
    path: "/playlists",
    component: Playlists
  },
  {
    path: "/search",
    component: MusicSearch
  },
  {
    path: "/home",
    name: "home",
    component: Home
  },
  {
    path: "/about",
    name: "about",
    // beforeEnter(to,from,next){ this.$root },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: "**",
    // component: PageNotFound
    redirect: "/playlists"
  }
];

const router = new VueRouter({
  // base:'/placki',
  // mode:'hash',
  linkActiveClass: "active",
  mode: "history",
  routes
});

export default router;
