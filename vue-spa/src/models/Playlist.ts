export interface Entity {
  id: number;
  name: string;
}

export interface Playlist extends Entity /* , OtherInterface */ {
  favorite: boolean;
  /**
   * HEX Color
   */
  color: string;
  tracks?: Track[];
  // entity_type:'playlist'
}

export interface Track extends Entity {
  // entity_type:'track'
}

// /* === */
// const x!: Playlist
// if (x.tracks) {
//     x.tracks.forEach
// }

// const x!: Playlist | Track;

// if(x.entity_type == 'playlist'){
//     x
// }

// switch(x.entity_type){
//     case  'playlist':
//     x
// }

// if('string' === typeof x.id){
//     x.id
// }

// var data: Playlist = { id: 123 }
