export interface Entity {
  id: string;
  name: string;
}

export interface Album extends Entity {
  type: "album";
  images: [AlbumImage?, AlbumImage?, AlbumImage?];
  artists?: Artist[];
}

export interface AlbumImage {
  url: string;
}

export interface Artist extends Entity {
  type: "artist";
  popularity?: number;
}

export interface PagingObject<T> {
  items: T[];
  total?: number;
  limit?: number;
}

export interface AlbumsResponse {
  albums: PagingObject<Album>;
}
