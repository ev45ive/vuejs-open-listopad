export default class AuthService {
  token: string | null = null;

  constructor(
    private auth_url: string,
    private client_id: string,
    private redirect_uri: string,
    private response_type = "token"
  ) {
    const rawToken = sessionStorage.getItem("token");
    if (rawToken) {
      this.token = JSON.parse(rawToken);
    }

    if (!this.token && window.location.hash) {
      const hash = window.location.hash.replace(/#|\//g, "");
      const params = new URLSearchParams(hash);
      this.token = params.get("access_token");

      if (this.token) {
        sessionStorage.setItem("token", JSON.stringify(this.token));
        window.location.hash = "";
      }
    }
  }

  authorize() {
    sessionStorage.removeItem("token");

    const url =
      `${this.auth_url}` +
      `?client_id=${this.client_id}` +
      `&redirect_uri=${this.redirect_uri}` +
      `&response_type=${this.response_type}`;
    //+`&show_dialog=true`

    // window.open(url)
    location.href = url;
  }

  getToken() {
    if (!this.token) {
      this.authorize();
    }
    return this.token;
  }
}
